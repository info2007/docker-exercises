# Network

In this scenario, we are going to deploy the CMS system called Wordpress.

So we need two containers:

- One container that can serve the Wordpress PHP files
- One container that can serve as a MySQL database for Wordpress.

Both containers already exists on the dockerhub: [Wordpress](https://hub.docker.com/_/wordpress/) and [MariaDB](https://hub.docker.com/_/mariadb/).

## the dirty way

```bash
docker container run --name mariadb-container --rm -p 3306:3306 -e MYSQL_ROOT_PASSWORD=wordpress -d mariadb
docker container run --name wordpress-container --rm -e WORDPRESS_DB_HOST=172.17.0.1 -e WORDPRESS_DB_PASSWORD=wordpress -p 8080:80 -d wordpress
```

Le's recap what those commands do:

- mariadb:
    - `docker` invokes the docker engine
    - `container` manage the container part of docker
    - `run` tells docker to run a new container off an image
    - `--name mariadb-container` gives the new container a name for better referencing
    - `--rm` tells docker to remove the container after it is stopped
    - `-p 3306:3306` mounts the host port 3306, to the containers port 3306.
    - `-e MYSQL_ROOT_PASSWORD=wordpress` The `-e` option is used to inject environment variables into the container.
    - `-d` runs the container detached, in the background.
    - `mariadb` tells what container to actually run, here mariadb:latest (:latest is the default if nothing else is specified)
- wordpress:
    - `-e WORDPRESS_DB_HOST=172.17.0.1 -e WORDPRESS_DB_PASSWORD=wordpress` pass-on environment varialbes.
    - `-p 8080:80` mounts the host port 8080 to the containers port 80 

We need to connect our wordpress container to the host's IP address.
You can either use the external IP address of your server, or the docker host IP `172.17.0.1`.

You can now browse to the IP:8080 and have your very own wordpress server running. Since port 3306 is the default MySQL port, wordpress will try to connect on that port by itself.

MySQL is now exposing it's port 3306 on the host, and everybody can attach to it **so do not do this in production without proper security settings**.

_it works but... can do better_


## Making a container network

```
docker network create if_wordpress
docker container run --name mysql-container --rm --network if_wordpress -e MYSQL_ROOT_PASSWORD=wordpress -d mariadb
docker container run --name wordpress-container --rm --network if_wordpress -e WORDPRESS_DB_HOST=mysql-container -e WORDPRESS_DB_PASSWORD=wordpress -p 8080:80 -d wordpress
```


Inspect the docker network to see what happened:

`docker network inspect if_wordpress`


